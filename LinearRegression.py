
import numpy as np


class LinearRegression:
    # error is calcualated using MSE
    # to get minimum MSE, use gradient descent
    def __init__(self, lr = 0.01, n_iters = 1000):
        self.lr = lr
        self.n_iters = n_iters
        self.weights = None
        self.bias = None


    def fit(self, X, y):
        n_samples, n_features = X.shape # getting the number features in dataset
        self.weights = np.zeros(n_features) # initializing weights to the number of features
        self.bias = 0 # initializing bias to 0

        for _ in range(self.n_iters):
            y_pred = np.dot(X, self.weights + self.bias) # predicting the results

            dw = (1/n_samples)*np.dot(X.T,(y_pred-y)) # calculating the derivatives for weight and bias
            db = (1/n_samples)*np.sum(y_pred-y)

            self.weights = self.weights - self.lr * dw # updating weights and bias
            self.bias = self.bias - self.lr * db


    def predict(self, X):
        y_pred = np.dot(X, self.weights + self.bias) # predicting the results
        return y_pred

